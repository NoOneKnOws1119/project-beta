# CarCar

Team:

* Person 1 - Nathan Grimes - Service
* Person 2 - Anton Zaitsev - Sales

## Design

For my models I used the following:
Technician
Appointment
AutomobileVO
I used the technician class to give data to the web page to define what technian would be working on which vehicle.
I used the appointment class for all of the details of an appointment such as: date, time, technician, vin number, customer name, and VIP status.
The automobileVO class I used to pulll the vin from the excisting data in the inventory to tell if the automobile was bouught from the dealership.

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

- The AutomobileVO model communicate directly with the Inventory model
via a poller to create instance of a current automobile inside the inventory microservice. This instance is used to determine where the Customer or Salesman model can use a specific instanace of an AutomovileVO's vin and href property, to create a sale.

- The Salesman model contains a salesman name property and an employee number property.

- The Customer model contains a customer name, adress, and phone number property.

- The Sale model contats a salesman property, which is a foreign key to the Salesman model, a customer property, which is a foreign key to the Customer model, and an automobile property which is a foreign key to the AutomobileVO, and a price property.
