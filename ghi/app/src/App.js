import { Routes, Route } from 'react-router-dom';
import MainPage from './routes/MainPage.js';
import Service_Form from './routes/service/Service_Form.js';
import Service_List from './routes/service/Service_List.js';
import Service_History from './routes/service/Service_History.js';
import Layout from './components/Layout';
import Automobile_List from './routes/inventory/Automobile_List.js';
import Automobile_Form from './routes/inventory/Automobile_Form.js';





function App() {
  return (
      <>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<MainPage />} />
            <Route path="/service/list" element={<Service_List />}/>
            <Route path="/service/create" element={<Service_Form />}/>
            <Route path="/service/history" element={<Service_History />}/>
            <Route path="/inventory/automobile/list" element={<Automobile_List />}/>
            <Route path="/inventory/automobile/create" element={<Automobile_Form />}/>
          </Route>
        </Routes>
      </>
  );
}

export default App;
