import { NavLink } from 'react-router-dom';



function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li>
              <a href="/service/list" className='nav-link'> Appointment List</a>
            </li>
            <li>
              <a href="/service/create" className='nav-link'>Create Appointment</a>
            </li>
            <li>
              <a href="/service/history" className="nav-link">Appointment History</a>
            </li>
            <li>
              <a href="/inventory/automobile/create" className='nav-link'> Create Automobile</a>
            </li>
            <li>
              <a href="/inventory/automobile/list" className="nav-link"> Automobile List</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
