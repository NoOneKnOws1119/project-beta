import Nav from '../Nav';
import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <header>
      <div className="nav-area">
        <Link to="/" className="logo">
          CarCar
        </Link>
        <Nav />
      </div>
    </header>
  );
};

export default Header;
