export const menuItems = [
    {
        title: 'Home',
        url: '/',
    },
    {
        title: 'Sales',
        url: '/sales',
        submenu: [
            {
                title: 'Create New Sale',
                url: '/sales/create',
            },
            {
                title: 'Sales List',
                url: '/sales/list',
            },
        ]
    },
    {
        title: 'Service',
        url: '/service',
        submenu: [
            {
                title: 'Schedule Service Appointment',
                url: '/service/create',
            },
            {
                title: 'Service Appointment List',
                url: '/service/list',
            },
            {
                title: 'Service Appointment History',
                url: '/service/history',
            }
        ]
    },
    {
        title: 'Inventory',
        url: '/inventory',
        submenu: [
            {
                title: 'Automobile',
                url: '/inventory/automobile',
                submenu: [
                    {
                        title: 'Automobile List',
                        url: '/inventory/automobile/list',
                    },
                    {
                        title: 'Create Automobile',
                        url: '/inventory/automobile/create',
                    },
                ]
            },
            {
                title: 'Model',
                url:'/inventory/model',
                submenu: [
                    {
                        title: 'Model List',
                        url: '/inventory/model/list',
                    },
                    {
                        title: 'Create Model',
                        url: '/inventory/model/create',
                    }
                ]
            },
            {
                title: 'Manufacturer',
                url: '/inventory/manufacturer',
                submenu: [
                    {
                        title: 'Manufacturer List',
                        url: '/inventory/manufacturer/list',
                    },
                    {
                        title: 'Create Manufacturer',
                        url: '/inventory/manufacturer/create',
                    },
                ]
            },

        ]
    },
    {
        title: 'Customer',
        url: '/customer',
        submenu: [
            {
                title: 'Customer List',
                url: '/customer/list',
            },
            {
                title: 'Create Customer',
                url: '/customer/create',
            },
        ]
    },
    {
        title: 'Employee',
        url: '/employee',
        submenu: [
            {
                title: 'Employee List',
                url: '/employee/list',
            },
            {
                title: 'Technician',
                url: '/employee/technician',
                submenu: [
                    {
                        title: 'History',
                        url: '/employee/technician/history',
                    },
                    {
                        title: 'Create',
                        url: '/employee/technician/create',
                    },
                ]
            },
            {
                title: 'Sales Person',
                url: '/employee/sales_person',
                submenu: [
                    {
                        title: 'History',
                        url: '/employee/sales-person/history',
                    },
                    {
                        title: 'Create',
                        url: '/employee/sales-person/create',
                    },
                ]
            },
        ]
    },
]
