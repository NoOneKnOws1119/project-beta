import { useEffect, useState } from "react";
import { Link } from 'react-router-dom';

const Model_Form = () => {
    const [formData, setFormData] = useState({
        model_id: "",
        color: "",
        year: "",
        vin: "",
    })

    const [models, setModels] = useState([])

    useEffect(() => {
        const loadData = async () => {
            const url = 'http://localhost:8100/api/models/';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setModels(data.models);
            } else {
                console.log("Error")
            }
        }
        loadData();
    }, [])

    const handleFormChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value,
        })
    }

    const handleSubmit = async (e) => {
        e.preventDefault();

        const url = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {

            setFormData({
                model_id: "",
                color: "",
                year: "",
                vin: "",
            });
        }
    }

    return <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new Automobile</h1>
                <form onSubmit={handleSubmit} id="create-manufacturer-form">

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange}
                            value={formData.vin}
                            placeholder="vin"
                            required
                            type="text"
                            name="vin"
                            id="vin"
                            className="form-control"
                        />
                        <label htmlFor="vin">VIN of Automobile</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange}
                            value={formData.color}
                            placeholder="color"
                            required
                            type="text"
                            name="color"
                            id="color"
                            className="form-control"
                        />
                        <label htmlFor="color">Color of Automobile</label>
                    </div>

                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange}
                            value={formData.year}
                            placeholder="year"
                            required
                            type="text"
                            name="year"
                            id="year"
                            className="form-control"
                        />
                        <label htmlFor="year">Year of Automobile</label>
                    </div>

                    <div className="mb-3">
                        <select onChange={handleFormChange}
                            value={formData.model_id}
                            required
                            name="model_id"
                            id="model_id"
                            className="form-select"
                        >
                            <option value="">Model of Automobile</option>
                            {models.map(model => {
                                return (
                                    <option
                                        key={model.id}
                                        value={model.id}
                                    >
                                        {model.name}
                                    </option>
                                )
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create Automobile</button>
                    <Link to="/inventory/automobile/list">
                        <button className="btn btn-warning">Go to Automobile List</button>
                    </Link>
                </form>
            </div>
        </div>
    </div>
}

export default Model_Form;
