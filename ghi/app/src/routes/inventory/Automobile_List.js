import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const Automobile_List = () => {
  const [autos, setautomobile] = useState([])

  useEffect(() => {
    const loadData = async () => {
      const url = 'http://localhost:8100/api/automobiles/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setautomobile(data.autos);
      } else {
        console.log("Error");
      }
    }

    loadData()

  }, [])

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Inventory List</h1>
          <table className="table">
            <thead>
              <tr>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
              </tr>
            </thead>
            <tbody>
              {
                autos.map(c => <tr key={c.id}>
                  <td>
                    <Link to={`/inventory/automobile/detail/${c.vin}`}>{c.vin}</Link>
                  </td>
                  <td>{c.color}</td>
                  <td>{c.year}</td>
                  <td>{c.model["name"]}</td>
                  <td>{c.model["manufacturer"]["name"]}</td>
                </tr>)
              }
            </tbody>
          </table>
          <Link to="/inventory/automobile/create"><button className="btn btn-primary">Create a new Automobile</button></Link>
        </div>
      </div>
    </div>
  );
}

export default Automobile_List;
