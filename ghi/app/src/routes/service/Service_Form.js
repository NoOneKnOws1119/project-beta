import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function Service_Form() {
  const [formData, setFormData] = useState({
    date: "",
    time: "",
    service_reason: "",
    technician: "",
    customer_name: "",
    vin: "",
    vip: "",
  })

  const [technician, setTechnician] = useState([])

  useEffect(() => {
    const loadData = async () => {
      const url = 'http://localhost:8080/api/technician/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setTechnician(data.technician);
      } else {
        console.log("Error");
      }
    }

    loadData()

  }, [])

  const handleFormChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    })
  }

  const handleSubmit = async (e) => {
    e.preventDefault();

    const url = 'http://localhost:8080/api/appointment/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);
    if (response.ok) {

      setFormData({
        date: "",
        time: "",
        service_reason: "",
        technician: "",
        customer_name: "",
        vin: "",
        vip: "",
      });
    }
  }

  return <div className="row">
    <div className="offset-3 col-6">

      <h1>Create a new Service Appointment</h1>
      <form onSubmit={handleSubmit} id="create-service-appointment-form">

        <div className="form-floating mb-3">
          <input onChange={handleFormChange}
            value={formData.vin}
            placeholder="vin"
            required type="text"
            name="vin"
            id="vin"
            className="form-control"
          />
          <label htmlFor="date">VIN Number</label>
        </div>

        <div className="form-floating mb-3">
          <input onChange={handleFormChange}
            value={formData.date}
            placeholder="date"
            required type="date"
            name="date"
            id="date"
            className="form-control"
          />
          <label htmlFor="date">Date of Service Appointment</label>
        </div>

        <div className="form-floating mb-3">
          <input onChange={handleFormChange}
            value={formData.time}
            placeholder="time"
            required type="time"
            name="time"
            id="time"
            className="form-control"
          />
          <label htmlFor="time">Time of Service Appointment</label>
        </div>

        <div className="mb-3">
          <select onChange={handleFormChange}
            value={formData.technician}
            required
            name="technician"
            id="technician"
            className="form-select"
          >
            <option value="">Technician</option>
            {technician.map(technicians => {
              return (
                <option
                  key={technicians.id}
                  value={technicians.id}
                >
                  {technicians.technician_name}
                </option>
              )
            })}
          </select>
        </div>

        <div className="form-floating mb-3">
          <input onChange={handleFormChange}
            value={formData.customer}
            placeholder="Customer Name"
            required type="text"
            name="customer_name"
            id="customer_name"
            className="form-control"
          >


          </input>
        </div>


        <label htmlFor="service_reason">Reason for Service Appointment</label>
        <textarea onChange={handleFormChange}
          value={formData.service_reason}
          placeholder="Reason for Appointment"
          required
          name="service_reason"
          id="service_reason"
          className="form-control"
        ></textarea>


        <button className="btn btn-primary">Create Service Appointment</button>
        <Link to="/service/list">
          <button className="btn btn-warning">Go to Appointment List</button>
        </Link>
      </form>
    </div>
  </div>

}

export default Service_Form;
