import React, { useState, useEffect } from 'react';

const Service_History = () => {

    const [appointment, setAppointment] = useState([]);
    const [filterInputValue, setFilterInputValue] = useState('');


    const loadData = async () => {
        const url = "http://localhost:8080/api/appointment/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();

            setAppointment(
                filterAppointments(data.appointment, filterInputValue),
            );
        } else {
            console.log("Error fetching completed appointments");
        }
    };

    useEffect(() => {
        loadData();
    }, [filterInputValue]);

    const filterAppointments = (appointment, filterInputValue) => {
        return appointment.filter((appointment) => appointment.vin.includes(filterInputValue));
    }

    const handleFilterInputChange = (event) => {
        setFilterInputValue(event.target.value);
    }

    const uniqueVins = [...new Set(appointment.map((appointment) => appointment.vin))];
    console.log(uniqueVins);

    return (
        <div className="row">
            <div className="center col-12">
                <div className="shadow p-4 mt-4">
                    <h1>Service Appointment History</h1>
                    <select
                        onChange={handleFilterInputChange}
                        id={filterInputValue}
                    >
                        <option value="">All</option>
                        {uniqueVins.map((c) => (
                            <option value={c}>{c}</option>
                        ))}
                    </select>


                    <table className="table">
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>Customer Name</th>
                                <th>Date</th>
                                <th>Technician</th>
                                <th>Reason</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {appointment
                            .filter((appointment) => appointment.status_of_service === "completed")
                            .map((c) => (
                                <tr key={c.id}>
                                    <td>{c.vin}</td>
                                    <td>{c.customer_name}</td>
                                    <td>{c.date}</td>
                                    <td>{c.technician["technician_name"]}</td>
                                    <td>{c.service_reason}</td>
                                    <td>{c.status_of_service}</td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    )
}

export default Service_History;
