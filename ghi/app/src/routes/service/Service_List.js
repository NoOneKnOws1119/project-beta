import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

const Service_List = () => {

    const [appointment, setAppointment] = useState([])

    useEffect(() => {
        const loadData = async () => {
            const url = 'http://localhost:8080/api/appointment/';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setAppointment(data.appointment);
            } else {
                console.log("Error");
            }
        }

        loadData()

    }, [])
    const [status_of_service, setStatus] = useState([]);
    const updateStatus = async (id) => {

        const response = await fetch(`http://localhost:8080/api/appointment/${id}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ status_of_service }),
        });

        if (response.ok) {
            console.log("Status updated successfully!");
            window.location.reload();
        } else {
            console.log("Error updating status");
        }
    };

    const deleteAppointment = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointment/${id}`, {
            method: "DELETE",
        });

        if (response.ok) {
            console.log("Appointment deleted successfully!");
            window.location.reload();
        } else {
            console.log("Error deleting appointment");
        }
    };

    const completeAppointment = async (id) => {
        const response = await fetch(`http://localhost:8080/api/appointment/${id}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
            },
            body: JSON.stringify({ status_of_service: "completed" }),
        });
        if (response.ok) {
            console.log("Appointment completed successfully!");
            window.location.reload();
        } else {
            console.log("Error completing appointment");
        }
    };

    return (
        <>
            <div className="row">
                <div className="center col-12">
                    <div className="shadow p-4 mt-4">
                        <h1>Service Appointment List</h1>
                        <table className="table">
                            <thead>
                                <tr>

                                    <th>VIN</th>
                                    <th>Customer Name</th>
                                    <th>VIP</th>
                                    <th>Date</th>
                                    <th>Time</th>
                                    <th>Technician</th>
                                    <th>Reason</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    appointment
                                        .filter((appointment) => appointment.status_of_service !== "completed")
                                        .sort((a, b) => b.vip - a.vip)
                                        .map((c) =>
                                            <tr key={c.id}>
                                                <td>{c.vip ? '★' : ''}</td>
                                                <td>{c.vin}</td>
                                                <td>{c.customer_name}</td>
                                                <td>{c.date}</td>
                                                <td>{c.time}</td>
                                                <td>{c.technician["technician_name"]}</td>
                                                <td>{c.service_reason}</td>
                                                <select
                                                    name="status_of_service"
                                                    id="status_of_service"
                                                    onChange={(event) => setStatus(event.target.value)}
                                                >
                                                    <option value="waiting for technician">Waiting for Technician</option>
                                                    <option value="in bay">Car in Bay</option>
                                                    <option value="diagnostics">Preforming Diagnostics</option>
                                                    <option value="repairs">Preforming Repairs</option>
                                                    <option value="cleanup">Final Cleanup</option>
                                                </select>
                                                <button
                                                    type='radio'
                                                    className='btn btn-info options-outline'
                                                    onClick={() => updateStatus(c.id)}>Update Status</button>
                                                <td>
                                                    <button
                                                        type='radio'
                                                        className='btn btn-danger options-outline'
                                                        onClick={() => deleteAppointment(c.id)}
                                                    >Cancel</button>
                                                </td>
                                                <td>
                                                    <button
                                                        type='radio'
                                                        className='btn btn-success options-outline'
                                                        onClick={() => completeAppointment(c.id)}
                                                    >Complete</button>
                                                </td>

                                            </tr>)
                                }
                            </tbody>
                        </table>
                        <Link to="/service/create"><button className="btn btn-primary">Create a new Service Appointment</button></Link>
                        <Link to="/service/history"><button className="btn btn-primary">View all of the Completed Service Appointments</button></Link>
                    </div>
                </div>
            </div>
        </>
    );

}
export default Service_List;
