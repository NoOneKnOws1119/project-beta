from django.contrib import admin
from .models import Salesman, Customer, Sale
# Register your models here.
@admin.register(Salesman)
class SalesmanAdmin(admin.ModelAdmin):
    pass

@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    pass

@admin.register(Sale)
class SalesAdmin(admin.ModelAdmin):
    pass
