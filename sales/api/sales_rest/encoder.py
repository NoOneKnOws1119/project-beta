from common.json import ModelEncoder
import json
from common.json import ModelEncoder
import json
from .models import Salesman, Customer, Sale


class SalesmanEncoder(ModelEncoder):
    model = Salesman
    properties = [
        'salesman_name',
        'emp_num',
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'customer_name',
        'address',
        'phone_num',
    ]

class SaleEncoder(ModelEncoder):
    model = Sale
    properties = [
        'salesman',
        'customer',
        'automobile',
        'price',
    ]
    encoders = {
        'salesman': SalesmanEncoder(),
        'customer': CustomerEncoder(),
    }
