# Generated by Django 4.0.3 on 2023-01-26 01:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0003_sale_emp_num'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sale',
            name='emp_num',
        ),
        migrations.RemoveField(
            model_name='sale',
            name='vin',
        ),
    ]
