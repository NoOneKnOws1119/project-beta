from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200)
    name = models.CharField(max_length=200, null=True)

    def __str__(self):
        return f"{self.name}"


class Salesman(models.Model):
    salesman_name = models.CharField(max_length=200)
    emp_num = models.CharField(max_length=200, null=True)

    def __str__(self):
        return f"{self.salesman_name}"

class Customer(models.Model):
    customer_name = models.CharField(max_length=200)
    address = models.CharField(max_length=200, null=True)
    phone_num = models.CharField(max_length=200, null=True)

    def __str__(self):
        return f"{self.customer_name}"

class Sale(models.Model):
    salesman = models.ForeignKey(
        Salesman,
        related_name="sale",
        on_delete=models.CASCADE,
        null=True
        )
    customer = models.ForeignKey(
        Customer,
        related_name="purchase",
        on_delete=models.CASCADE,

        )
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="auto_sold",
        on_delete=models.CASCADE,
        )
    price = models.IntegerField()
