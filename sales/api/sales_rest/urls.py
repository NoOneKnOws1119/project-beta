from django.urls import path

from .views import api_salespreson_list, api_customer_list, api_sales_list

urlpatterns = [
    path('salesperson_list/', api_salespreson_list, name='api_salesperson_list'),
    path('customer_list/', api_customer_list, name='api_customer_list'),
    path('sales_list', api_sales_list, name="api_sales_list"),

]
