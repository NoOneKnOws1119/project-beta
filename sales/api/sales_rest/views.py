from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import Salesman, Customer, Sale
from .encoder import SalesmanEncoder, CustomerEncoder, SaleEncoder

# Create your views here.
@require_http_methods(["GET", "POST"])
def api_salespreson_list(request):
    if request.method == 'GET':
        salesperson = Salesman.objects.all()
        return JsonResponse(
            {'salesperson': salesperson},
             encoder=SalesmanEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesman.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalesmanEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_customer_list(request):
    if request.method == 'GET':
        customer = Customer.objects.all()
        return JsonResponse(
            {'customer': customer},
             encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_sales_list(request):
    if request.method == 'GET':
        sales = Sale.objects.all()
        return JsonResponse(
            {'sales': sales},
             encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        sales = Sale.objects.create(**content)
        return JsonResponse(
            sales,
            encoder=SaleEncoder,
            safe=False,
        )
