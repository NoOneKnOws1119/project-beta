from common.json import ModelEncoder
from.models import AutomobileVO, Technician, Appointment


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin',
        'owner_name',
        'purchased_from_dealer',
    ]


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        'technician_name',
        'id_number',
        'id',
    ]


class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
       "id",
        "date",
        "time",
        "service_reason",
        "status_of_service",
        "vin",
        "vip",
        "technician",
        "customer_name",
    ]
    encoders = {
        'technician':TechnicianEncoder(),

    }
