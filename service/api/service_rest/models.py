from django.db import models
from django.urls import reverse
from datetime import datetime


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17)

    def __str__(self):
        return f"{self.vin}"


class Technician(models.Model):
    id_number = models.CharField(max_length=200, unique=True)
    technician_name = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.technician_name}'

    def get_api_url(self):
        return reverse("api_technician", args=(self.id_number))


class Appointment(models.Model):
    date = models.DateField()
    time = models.TimeField()
    technician = models.ForeignKey(Technician, related_name='appointment', on_delete=models.PROTECT)
    service_reason = models.CharField(max_length=100)
    status_of_service = models.CharField(max_length=200, default="Waiting For Technician")
    vin = models.CharField(max_length=17, default=None)
    customer_name = models.CharField(max_length=200)
    vip = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.customer_name}"

    def get_api_url(self):
        return reverse("api_service_history", kwargs={"vin": self.vin})
