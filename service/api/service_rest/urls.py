from django.urls import path

from .views import api_appointment, api_technician_list, api_technician, api_appointment_detail, api_appointment_history

urlpatterns = [
    path('appointment/history/<str:vin>', api_appointment_history, name='api_appointment_history'),
    path('appointment/<str:id>', api_appointment_detail, name='api_appointment_detail'),
    path('technician/<str:id_number>/', api_technician, name='api_technician'),
    path('technician/', api_technician_list, name='api_technician_list'),
    path('appointment/', api_appointment, name='api_appointment'),
]
