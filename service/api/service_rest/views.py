from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


from .models import AutomobileVO, Appointment, Technician
from .encoder import  TechnicianEncoder, AppointmentEncoder

@require_http_methods(["GET", "POST"])
def api_technician_list(request):
    if request.method == 'GET':
        technician = Technician.objects.all()
        return JsonResponse(
            {'technician': technician},
             encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST", "DELETE"])
def api_technician(request, id_number):
    if request.method == 'GET':
        try:
            technician = Technician.objects.get(id_number=id_number)
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({'message': "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == 'DELETE':
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({'message': "Does not exist"})
    else:
        try:
            content - json.loads(request.body)
            technician= Technician.object.get(id=id)
            props = ["id_number", "technician_name"]
            for prop in props:
                if prop in content:
                    setattr(technician, prop, content[prop])
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods(["GET", "POST"])
def api_appointment(request):
    if request.method == 'GET':
        appointment = Appointment.objects.all()
        return JsonResponse(
            {'appointment': appointment},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        technician_id_number = content['technician']
        technician = Technician.objects.get(id=technician_id_number)
        content['technician'] = technician
        try:
            vin = content['vin']
            AutomobileVO.objects.get(vin=vin)
            content['vip'] = True
        except AutomobileVO.DoesNotExist:
            pass
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_appointment_detail(request, id):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(id=id)

            props = [
                "date",
                "reason",
                "automobile",
                "technician",
                "status_of_service",
                ]
            for prop in props:
                if prop in content:
                    setattr(appointment, prop, content[prop])
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods("GET")
def api_appointment_history(request, vin):
    if request.method == "GET":
        try:
            appointment = Appointment.objects.all().filter(vin=vin)
            return JsonResponse(
                {"appointment": appointment},
                encoder=(AppointmentEncoder),
                safe=False
                )
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "This vehicle has no past or future appointments"}
                )
            response.status_code = 404
            return response
